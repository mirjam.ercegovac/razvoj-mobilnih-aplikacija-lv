package hr.ferit.ercegovac.todo.data
import hr.ferit.ercegovac.todo.R
import hr.ferit.ercegovac.todo.model.Task

class DataSource {
    fun loadTasks () : List<Task>{
        return listOf<Task>(
            Task(R.string.task1),
            Task(R.string.task2),
            Task(R.string.task3),
        )
    }
}