package hr.ferit.ercegovac.todo.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import hr.ferit.ercegovac.todo.R
import androidx.recyclerview.widget.RecyclerView
import hr.ferit.ercegovac.todo.model.Task

class TaskAdapter (val dataSource : List<Task>,
                   val context : Context)
    : RecyclerView.Adapter<TaskAdapter.ItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val layoutItem = LayoutInflater.from(parent.context).inflate(R.layout.item_list, parent, false)
        return ItemViewHolder(layoutItem)

    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val task = dataSource[position]
        holder.textView.text = context.resources.getString(task.stringResId)
    }

    override fun getItemCount(): Int {
        return dataSource.size
    }

    class ItemViewHolder(private val view : View) : RecyclerView.ViewHolder(view){
        val textView = view.findViewById<TextView>(R.id.textView)
    }
}