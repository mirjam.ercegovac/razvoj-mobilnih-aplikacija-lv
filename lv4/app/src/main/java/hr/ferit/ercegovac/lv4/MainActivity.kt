package hr.ferit.ercegovac.lv4

import android.media.AudioManager
import android.media.SoundPool
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import hr.ferit.ercegovac.lv4.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    private lateinit var mSoundPool: SoundPool
    private var mLoaded: Boolean = false
    var mSoundMap: HashMap<Int, Int> = HashMap()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        this.setUpUi()
        this.loadSounds()
    }

    private fun setUpUi() {
        binding.playAirplane.setOnClickListener({ playSound(R.raw.airplane) })
        binding.playCar.setOnClickListener({ playSound(R.raw.car) })
        binding.playWaterfall.setOnClickListener({ playSound(R.raw.car) })
    }

    private fun loadSounds() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.mSoundPool = SoundPool.Builder().setMaxStreams(10).build()
        } else {
            this.mSoundPool = SoundPool(10, AudioManager.STREAM_MUSIC, 0)
        }
        this.mSoundPool.setOnLoadCompleteListener { _, _, _ -> mLoaded = true }
        this.mSoundMap[R.raw.airplane] = this.mSoundPool.load(this, R.raw.airplane, 1)
        this.mSoundMap[R.raw.car] = this.mSoundPool.load(this, R.raw.car, 1)
        this.mSoundMap[R.raw.waterfall] = this.mSoundPool.load(this, R.raw.waterfall, 1)
    }

   fun onClick(v: View) {
        if (this.mLoaded == false) return
        when (v.getId()) {
            R.id.playAirplane -> playSound(R.raw.airplane)
            R.id.playCar -> playSound(R.raw.car)
            R.id.playWaterfall -> playSound(R.raw.waterfall)
        }
    }

    fun playSound(selectedSound: Int) {
        val soundID = this.mSoundMap[selectedSound] ?: 0
        this.mSoundPool.play(soundID, 1f, 1f, 1, 0, 1f)
    }


}